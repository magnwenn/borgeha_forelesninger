import numpy as np
vektor = np.array([1,2,3,4,5])
vec1 = vektor*4
vec2 = vektor**2
vec3 = vektor<3
vec4 = 10*np.sin(vektor)
print(f'\nvektor = {vektor}\n')
print(f'vec1 = {vec1}')
print(f'vec2 = {vec2}')
print(f'vec3 = {vec3}')
print(f'vec4 = {vec4}')
