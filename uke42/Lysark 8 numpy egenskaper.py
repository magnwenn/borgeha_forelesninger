import numpy as np

matrise = np.ones((3,2))
print(f'\n{matrise}\n')
print(f'Matrisens dimensjon er {matrise.ndim}')
print(f'Antall linjer er {matrise.shape[0]} og antall kolonner {matrise.shape[1]}')
print(f'Antall elementer i matrisen er {matrise.size}')
print(f'Matrisen inneholder elementer med datatype {matrise.dtype}')