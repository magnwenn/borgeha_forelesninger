# Spør om navnet
# Spør om alder
# hvis 18 eller over spør du
# om personen har vasket hendene,
# som 'Har du vaska henda, Birgitte?'
# hvis svaret er ja, ønsk henne velkommen inn.
# hvis hun ikke har vasket seg ber du henne
# om å gjøre det og komme tilbake
# hvis hun ikke er 18 eller over, svar at
# hun er for ung

# Hvordan skal en dele opp dette best mulig?
# Kan vi se noen tester som er inni andre tester?

navn = input('navn: ')
alder = int(input('alder: '))

if alder >= 18:
    vasket_hendene = input(f'Har du vasket hendene,{navn}: ')
    if 'ja' in vasket_hendene:
        print('Velkommen,', navn)
    else:
        print('Vask vask, vask vaske henda.')
        
#    print('gammel nok')
else:
    print(f'Du er ikke gammel nok {navn}.')